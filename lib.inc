%define READ_CALL 0
%define WRITE_CALL 1
%define EXIT_CALL 60

%define SYSTEM_IN 0
%define SYSTEM_OUT 1
%define SYSTEM_ERROR 2

section .text
 
exit:
	mov rax, EXIT_CALL
	syscall

string_length:
	xor rax, rax
	.loop:
		cmp byte[rdi + rax], 0
		je .end
		inc rax
		jmp .loop
	.end:
		 ret

print_string:
	push rdi
	call string_length
	pop rsi
	mov rdx, rax
	mov rdi, SYSTEM_OUT
	mov rax, WRITE_CALL
	syscall
	ret

print_newline:
	mov rdi, `\n`

print_char:
	push rdi
	mov rax, WRITE_CALL
	mov rdi, SYSTEM_OUT
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rdi
	ret

print_int:
	test rdi, rdi
	jge print_uint
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
 
print_uint:
	test rdi, rdi
	jne .not_zero
	mov rdi, '0'
	jmp print_char
	.not_zero:
		sub rsp, 24
		mov rcx, 22
		mov rsi, 10
		mov rax, rdi
		.loop:
			xor rdx, rdx
			div rsi
			add dl, 48
			mov byte[rsp + rcx], dl
			test rax, rax
			je .end
			dec rcx
			jmp .loop
		.end:
			mov byte[rsp + 23], 0
			lea rdi, [rsp + rcx]
			call print_string
			add rsp, 24
			ret

string_equals:
	xor r8, r8
	.loop:
		mov cl, byte[rdi + r8]
		cmp cl, byte[rsi + r8]
		jne .no
		cmp cl, 0
		je .end
		inc r8
		jmp .loop
	.end:
		mov rax, 1
		ret
	.no:
		xor rax, rax
		ret

read_char:
	push 0
	xor rax, rax
	xor rdi, rdi
	mov rsi, rsp
	mov rdx, 1
	syscall
	test rax, rax
	jz .end_or_err
	js .end_or_err
	pop rax
	ret
	.end_or_err:
		pop rax
		xor rax, rax
		ret
		
read_word:
	push rbx
	push r12
	push r13
	xor rbx, rbx
	mov r12, rdi
	mov r13, rsi
	.skip:
		call read_char
		cmp al, `\t`
		je .skip
		cmp al, `\n`
		je .skip
		cmp al, ` `
		je .skip
	.loop:
		cmp rbx, r13
		jge .err
		test al, al
		je .done
		cmp al, `\t`
		je .done
		cmp al, `\n`
		je .done
		cmp al, ` `
		je .done
		mov byte[r12 + rbx], al
		inc rbx
		call read_char
		jmp .loop
	.done:
		mov byte[r12 + rbx], 0
		mov rax, r12
		mov rdx, rbx
		jmp .end
	.err:
		xor rax, rax
	.end:
		pop r13
		pop r12
		pop rbx
		ret

parse_uint:
	xor rax, rax
	xor r8, r8
	xor r9, r9
	mov rsi, 10
	.loop:
		mov r8b, byte[rdi + r9]
		cmp r8, '9'
		jg .stop
		sub r8, '0'
		jl .stop
		add rax, r8
		inc r9
		mul rsi
		jmp .loop
	.stop:
		xor rdx, rdx
		test r9, r9
		jnz .complete
		ret
	.complete:
		div rsi
		mov rdx, r9
		ret

parse_int:
	mov cl, byte[rdi]
	cmp cl, '-'
	je .sign
	cmp cl, '+'
	jne parse_uint
	.sign:
		push rcx
		inc rdi
		call parse_uint
		test rdx, rdx
		jne .ok
		pop rcx
		ret
	.ok:
		pop rcx
		cmp cl, '-'
		jne .positive
		neg rax
	.positive:
		inc rdx
		ret 

string_copy:
	push rdi
	push rsi
	push rdx
	call string_length
	pop rdx
	pop rsi
	pop rdi
	cmp rax, rdx
	jg .overflow
	xor r8, r8
	.loop:
		mov cl, byte[rdi + r8]
		mov byte[rsi + r8], cl
		test cl, cl
		je .end
		inc r8
		jmp .loop
	.overflow:
		xor rax, rax
		ret
	.end:
		ret

;kek
